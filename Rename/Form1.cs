﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rename
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            // Display the openFile dialog.
            DialogResult result = folderBrowserDialog1.ShowDialog();

            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog1.SelectedPath;
            }
          
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            var path = txtFolder.Text.Trim();
            if (path.Length == 0)
            {
                MessageBox.Show("Please select folder contains files need to replace");
                return;
            }
            //check if the selected folder is existing or not
            if (!Directory.Exists(path))
            {
                MessageBox.Show("Please select correct folder contains files need to replace");
                return;
            }
            //
            var oldString = txtOldString.Text;
            var newString = txtNewString.Text;
            DialogResult result = MessageBox.Show(
                string.Format("Do you really want to replace {0} by {1}", oldString, newString), "Confirm",
                MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel)
            {
                return;
            }
           
            //Replace string inside files and the rename
            ReplaceString(path, oldString, newString);
            RenameFiles(path, oldString, newString);
            RenameFolder(path, oldString, newString);
            txtFolder.Text = txtOldString.Text = txtNewString.Text = string.Empty;
            MessageBox.Show("Done");
        }

        public void ReplaceString(string folder, string oldString, string newString)
        {
            var files = Directory.GetFiles(folder);
            foreach (var file in files)
            {
                try
                {
                    string content = System.IO.File.ReadAllText(file);
                    content = content.Replace(oldString, newString);
                    System.IO.File.WriteAllText(file,content);
                }
                catch 
                {
                    
                }
                
            }
            var childFolders = Directory.GetDirectories(folder);
            foreach (var childFolder in childFolders)
            {
                ReplaceString(childFolder, oldString, newString);
            }
        }

        public void RenameFiles(string folder, string oldString, string newString)
        {
            var files = Directory.GetFiles(folder);
            foreach (var file in files)
            {
                try
                {
                    string oldFileName = Path.GetFileName(file);
                    string newFileName = oldFileName.Replace(oldString, newString);
                    string newFilePath = string.Format("{0}/{1}", folder, newFileName);
                    if (!file.Equals(newFilePath))
                    {
                        File.Move(file, newFilePath);
                    }
                    
                }
                catch
                {

                }

            }
            var childFolders = Directory.GetDirectories(folder);
            foreach (var childFolder in childFolders)
            {
                RenameFiles(childFolder, oldString, newString);
            }
            
        }
        public void RenameFolder(string folder, string oldString, string newString)
        {
            string oldFileName = Path.GetFileName(folder);
            string newFileName = oldFileName.Replace(oldString, newString);
            string newFilePath = string.Format("{0}\\{1}",Path.GetDirectoryName(folder) , newFileName);
            if (!folder.Equals(newFilePath))
            {
                Directory.Move(folder, newFilePath);
            }
            var childFolders = Directory.GetDirectories(newFilePath);
            foreach (var childFolder in childFolders)
            {
                RenameFolder(childFolder, oldString, newString);
            }

        }

        private void btnExportFolderName_Click(object sender, EventArgs e)
        {
            //Export folder name to csv 
            var path = txtFolder.Text.Trim();
            if (path.Length == 0)
            {
                MessageBox.Show("Please select folder contains files need to replace");
                return;
            }
            //check if the selected folder is existing or not
            if (!Directory.Exists(path))
            {
                MessageBox.Show("Please select correct folder contains files need to replace");
                return;
            }

            var childFolders = Directory.GetDirectories(path);
           
            StringBuilder sb = new StringBuilder();
            var folderNameList = new List<string>();
            foreach (var childFolder in childFolders)
            {
                folderNameList.Add(Path.GetFileName(childFolder));//get folder name
            }
            //folderNameList sort asc 
            folderNameList = folderNameList.OrderBy(x => x).ToList();
            foreach (var s in folderNameList)
            {
                sb.AppendLine(s);
            }
            var exportFileName = $"{path}\\folder-name-{Guid.NewGuid().ToString()}.txt";
            System.IO.File.WriteAllText(exportFileName, sb.ToString());
            MessageBox.Show($"{folderNameList.Count()} found.");
        }

        private void btn_Compare_Click(object sender, EventArgs e)
        {
            var f = new FrmCompare();
            f.ShowDialog();
        }
    }
}
