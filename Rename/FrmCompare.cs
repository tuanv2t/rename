﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Rename
{
    public partial class FrmCompare :Form
    {
        public FrmCompare()
        {
            InitializeComponent();
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFile1.Text))
            {
                MessageBox.Show("Please select File 1");
                return;
            }
            if (string.IsNullOrEmpty(txtFile2.Text))
            {
                MessageBox.Show("Please select File 2");
                return;
            }

            var file1Data =  System.IO.File.ReadAllLines(txtFile1.Text).ToList();
            var file2Data = System.IO.File.ReadAllLines(txtFile2.Text).ToList();
            var diff = new List<string>();
            //File1 has but File2 does not have 
            foreach (var s in file1Data)
            {
                if(!file2Data.Any(x=>x==s))
                {
                    diff.Add(s);
                }
            }
            foreach (var s in diff)
            {
                txtResult.Text+=s + Environment.NewLine;
            }
              
        }
    }
}
